FROM ruby:3.1.0-bullseye as base

# Set the working directory
WORKDIR /atmosphere

# Install dependencies
RUN apt-get update && \
  apt-get install -y \
  build-essential \
  nodejs \
  postgresql-client && \
  rm -rf /var/lib/apt/lists/*

RUN  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash && \
 . ~/.nvm/nvm.sh && \
  nvm install 16.16.0 && \
  nvm alias default 16.16.0

# Install gems
COPY Gemfile Gemfile.lock ./
RUN bundle install --jobs 20 --retry 5 --path /usr/local/bundle

# Copy the application code
COPY . .

# Expose ports
EXPOSE 3000

RUN useradd -m vitoruser
USER vitoruser
HEALTHCHECK CMD curl --fail http://localhost:4000 || exit 1

CMD ["bin/rails", "server", "-b", "0.0.0.0"]
